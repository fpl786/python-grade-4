'''
write a python program that takes 2 numbers as input and multiplies them
together

Modify this to do addition, subtraction, multiplication and division


Eg.

Input:
Enter the first number:
4
Enter the second number:
2

Output:
4 * 2 = 8
4 / 2 = 2
4 + 2 = 6
4 - 2 = 2

input function - waits for user to type in information, and provides the
information to the program

int function - converts information into a number



'''


number1 = int(input("Enter the first number: "))

number2 = int(input("Enter the second number: "))


print(number1, "*", number2,"=", number1*number2)
print(number1, "+", number2,"=", number1+number2)
print(number1, "-", number2,"=", number1-number2)
print(number1, "/", number2,"=", number1/number2)



