'''

build a basic calculator program

Ask the user for the first number

Ask the user for the operation (+, - , *, /)

Ask the user for the second number

Evaluate the expression and print the result

Eg.
Type the first number
6
Type the operation
+
Type the second number
5

11
'''

number1 = int(input("Enter the first number: "))

operation = input("Enter the operation: ")

number2 = int(input("Enter the second number: "))


if operation == "+":
    print(number1, "+", number2, "=", number1+number2)
if operation == "-":
    print(number1, "-", number2, "=", number1-number2)
if operation == "*":
    print(number1, "*", number2, "=", number1*number2)
if operation == "/":
    print(number1, "/", number2, "=", number1/number2)

