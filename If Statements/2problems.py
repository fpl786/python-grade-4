'''
Problem 1:
take in a number is input
print "positive" if the number is positive
and "negative" if the number is negative
if the number is 0 than print "neutral"
'''

'''
Problem 2:
take in a number as input
print "even" if the number is even
and "odd" if the number is odd
HINT: Use the % operator
'''


number = int(input("Enter a number: "))


if number > 0:
    print("Positive!")
elif number < 0:
    print("Negative")
else:
    print("Neutral")
