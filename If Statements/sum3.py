'''
write a program that takes 3 numbers as input and adds
them together. If the number is greater than 15 print "sum is large"
otherwise print "sum is small"
'''

number1 = int(input("Enter the first number: "))

number2 = int(input("Enter the second number: "))

number3 = int(input("Enter the third number: "))

s = number1 + number2 + number3

print("Sum:", s)

if s > 15:
    print("Sum is greater than 15")
elif s < 15:
    print("Sum is less than 15")
else:
    print("Sum is equal to 15")
