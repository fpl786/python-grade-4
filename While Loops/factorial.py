'''
Write a program takes a number as input and
calculates the factorial of the number

USE A WHILE LOOP

'''

number = int(input("Enter a number: "))

counter = 1 

factorial = 1


while counter <= number:
    factorial *= counter
    counter += 1
    
print(factorial)
