'''

write a python program that sums up all the numbers from 1 to 1000
USE A WHILE LOOP

'''

counter  = 0
s = 0

while counter <= 1000:
    s += counter
    counter += 1

print(s)
